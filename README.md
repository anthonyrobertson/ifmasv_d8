
the mysql containers are each deigned to host ONE database
( /docker-entrypoint-initdb.d/ only does one )

https://hub.docker.com/_/mysql/


The volumes used for the mysql data mean that db data will persist between container restarts.

This also means that dbs will only import on first container run, if re-import is needed then remove the volume before restarting 

NOTE: the format of the sql dump file will determine how the new db is imported
 * If the SQl contains 'create' statements, then the db is going to get created with whatever name is speficied
 * IF the SQL does not have 'create' statements, then the db name defined in docker-compose.yml  MYSQL_DATABASE 

( details: https://stackoverflow.com/questions/25920029/setting-up-mysql-and-importing-dump-within-dockerfile )

Put your legacy database into ./db_legacy
Put your progress/snapshot (drupal8 db) into ./db_progress

Pull in code from the git repository

Start Everything:
docker-compose up -d 
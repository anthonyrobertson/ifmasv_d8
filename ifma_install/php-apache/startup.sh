#!/bin/bash
chown -R www-data:www-data /var/www/html/sites/default/files
chown -R www-data:www-data /var/tmp
chmod 770 /var/tmp

cd /var/www/html/sites/default
# rm settings.php
cp ./settings.$envType.php settings.php


# finally start apache
exec apache2-foreground

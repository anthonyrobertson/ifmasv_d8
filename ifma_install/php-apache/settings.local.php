<?php

$databases = [];
$config_directories = [];

$settings['hash_salt'] = 'YavVxoI2nKPzNzXWqcq_TeecTg9blY3xfJJ5jXWu0ARUD3NSzwPhXgmtrrweD8A70Ay6y-R62Q';
$settings['update_free_access'] = FALSE;

$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/development.services.yml';

$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

// $settings['ifmaEnv'] = 'dev'; // use envType instead


// if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
// 	include $app_root . '/' . $site_path . '/settings.local.php';
// }

$databases['default']['default'] = array (
  'database' => 'drupal8',
  'username' => 'root',
  'password' => 'drupal_db_pass',
  'prefix' => '',
  'host' => 'db',
  'port' => '',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

$settings['install_profile'] = 'minimal';
$config_directories['sync'] = '/var/www/config/sync';


// Not necessary, all 'dev' services config is copied from services.dev.yml -> services.yml
// The following is only needed if you want to add even more files ( unsure how merging may work )
//$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/services.dev.yml';

$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

// Disable caches
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
$settings['cache']['bins']['page'] = 'cache.backend.null';

ini_set('opcache.enable', 0);

// Error Reporting
$config['system.logging']['error_level'] = 'verbose';
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);

ini_set('memory_limit', '1024M');

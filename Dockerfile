# from https://www.drupal.org/docs/8/system-requirements/drupal-8-php-requirements
FROM php:7.2-apache

# install the PHP extensions we need
RUN set -ex; \
	\
	if command -v a2enmod; then \
		a2enmod rewrite; \
	fi; \
	\
	savedAptMark="$(apt-mark showmanual)"; \
	\
	apt-get update; \
	apt-get install -y --no-install-recommends \
		libjpeg-dev \
		libpng-dev \
		libpq-dev \
	; \
	\
	docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
	docker-php-ext-install -j "$(nproc)" \
		gd \
		opcache \
		pdo_mysql \
		pdo_pgsql \
		zip \
	; \
	\
# reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
	apt-mark auto '.*' > /dev/null; \
	apt-mark manual $savedAptMark; \
	ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
		| awk '/=>/ { print $3 }' \
		| sort -u \
		| xargs -r dpkg-query -S \
		| cut -d: -f1 \
		| sort -u \
		| xargs -rt apt-mark manual; \
	\
	apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
	rm -rf /var/lib/apt/lists/*

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

RUN apt-get update && apt-get install -y \
	curl \
	git \
	mysql-client \
	nano \
	wget

RUN cd /var/www/html
WORKDIR /var/www/html

# https://www.drupal.org/node/3060/release
ENV DRUPAL_VERSION 8.8.1
ENV DRUPAL_MD5 0e0af2652e6ad4da27c0f7bf35c5e1e1


RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
	php composer-setup.php && \
	mv composer.phar /usr/local/bin/composer && \
	php -r "unlink('composer-setup.php');"


RUN curl -fSL "https://ftp.drupal.org/files/projects/drupal-${DRUPAL_VERSION}.tar.gz" -o drupal.tar.gz \
	&& echo "${DRUPAL_MD5} *drupal.tar.gz" | md5sum -c - \
	&& tar -xz --strip-components=1 -f drupal.tar.gz \
	&& rm drupal.tar.gz \
	&& chown -R www-data:www-data sites modules themes

RUN composer install

RUN echo 'memory_limit = -1' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini
RUN php -r "echo ini_get('memory_limit').PHP_EOL;"

RUN composer require 'drush/drush:^9.0.0'
RUN composer require 'drupal/migrate_upgrade'
RUN composer require 'drupal/migrate_tools'
RUN composer require 'drupal/group:1.x-dev'


ENV envType local

COPY ./ifma_install/php-apache/settings.local.php /var/www/html/sites/default/settings.local.php

ENV PATH "$PATH:/var/www/html/vendor/bin"


# -----------------------------------------------------------------------------
# PHP INI Modifications / Overrides
# -----------------------------------------------------------------------------

# RUN sed -i -e 's/^.*memory_limit\s*=.*/memory_limit=1024M/g' \
#      -e 's/^.*max_execution_time\s*=.*/max_execution_time=120/g' \
#      -e 's/^.*post_max_size\s*=.*/post_max_size=32M/g'  \
#      -e 's/^.*upload_max_filesize\s*=.*/upload_max_filesize=32M/g'  \
#      -e 's/^.*opcache.enable\s*=.*/opcache.enable=0/g'  \
#      -e 's/^.*disable_functions\s*=.*/disable_functions=exec,eval,phpinfo,shell_exec,system/g'  \
#      -e 's/^.*allow_url_fopen\s*=.*/allow_url_fopen=Off/g'  \
#      -e 's/^.*file_uploads\s*=.*/file_uploads=On/g'  \
#      -e 's/^.*open_basedir\s*=.*/open_basedir=\/var/g'  \
#      -e 's/^.*sys_temp_dir\s*=.*/sys_temp_dir=\/var\/tmp/g'  \
#      -e 's/^.*error_log\s*=.*/error_log=\/dev\/stderr/g'  \
#      -e 's/^.*catch_workers_output\s*=.*/catch_workers_output=1/g'  \
#      -e 's/^.*access.log\s*=.*/access.log=\/dev\/stdout/g'  \
#      -e 's/^.*session.name\s*=.*/session.name=HEREPHPSessionID/g' /etc/php.ini

# RUN cp /etc/php.ini /etc/php-cli.ini

# RUN sed -i -e 's/^.*disable_functions\s*=.*/disable_functions=/g'  \
#         -e 's/^.*open_basedir\s*=.*/open_basedir=/g'  /etc/php-cli.ini

# APPLY PATCH - ( supports the nodereference_url field type mapping to entityreference )
COPY ./ifma_install/d6_field.yml /var/www/html/core/modules/field/migrations/d6_field.yml
COPY ./ifma_install/d6_field_instance_widget_settings.yml /var/www/html/core/modules/field/migrations/d6_field_instance_widget_settings.yml

# THE MIGRATION SOURCE FILES ( uploaded user content files )
# the migration task will look for these as a local source of the files
RUN mkdir -p /var/ifma_src_files/sites/default/files
COPY ./ifma_install/files_to_migrate.tar.gz /var/ifma_src_files/files_to_migrate.tar.gz
RUN tar -xzf /var/ifma_src_files/files_to_migrate.tar.gz -C /var/ifma_src_files/sites/default/files
RUN	rm /var/ifma_src_files/files_to_migrate.tar.gz

# -----------------------------------------------------------------------------
# VOLUME work
# -----------------------------------------------------------------------------
VOLUME /var/www/html/sites/default/files
RUN mkdir /var/www/private
VOLUME /var/www/private

# needed for using 'cache.backend.null' in development
RUN cp /var/www/html/sites/development.services.yml \
	/var/www/html/sites/default/development.services.yml

# RUN sed -i -e "s/DB_PASS_PLACEHOLDER/$dbPass/g" settings.php

# -----------------------------------------------------------------------------
# Final CMD / Entrypoint
# -----------------------------------------------------------------------------

COPY ./ifma_install/php-apache/startup.sh /startup.sh
# RUN dos2unix /startup.sh
RUN chmod +x /startup.sh
CMD ["/startup.sh"]


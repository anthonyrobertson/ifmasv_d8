#!/bin/bash

echo "Enter snapshot filename to be saved in ./db_backups :"

read BACKUP_NAME

docker exec db /usr/bin/mysqldump -u root --password=drupal_db_pass drupal8 > ./db_backups/$BACKUP_NAME.sql